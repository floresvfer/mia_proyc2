$( document ).ready(function() {
  fillHexagonsData();
});


function fillHexagonsData(){
  $.ajax({
    type: "GET",
    url: "../responses/PLAYERS",
    data: {id: 1},
    dataType: "json",
    success: function(result){
        var html = "";
        var json = JSON.parse(result);
        html += "<li style=\"cursor: pointer;\" onclick=\"JavaScript:goTo('./newCoach')\">";
        html += "    <div>";
        html += "      <img src=\"../static/images/add.png\" alt=\"\"/>";
        html += "      <h1>N E W</h1>";
        html += "      <p>PLAYER</p>";
        html += "  </div>";
        html += "</li>";
        for(var i=0; i<json.length; i++){
            html += "<li style=\"cursor: pointer;\" onclick=\"JavaScript:goTo('')\">";
            html += "    <div>";
            html += "      <img src=\"../static/files/coaches/"+(i+1)+".jpg\" alt=\"\"/>";
            html += "      <h1>"+json[i].fields['NOMBRE']+" "+json[i].fields['APELLIDO']+"</h1>";
            html += "      <p>"+json[i].fields['FECHANACIMIENTO']+"</p>";
            html += "  </div>";
            html += "</li>";
        }
        $('#categories').html("");
        $('#categories').html(html);
    }
  });
}
