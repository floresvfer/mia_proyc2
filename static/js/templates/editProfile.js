$(document).ready(function () {
    $('#btnRegister').click(function () {
        register();
    });
});

function register() {
    let data = new FormData($('#contactForm').get(0));
    $.ajax({
       type: 'POST',
       url: '../responses/UPDATEPROFILE',
       data: data,
        cache: false,
        processData: false,
        contentType: false,
       success: function (response) {
            alert(response);
            window.location.href = './profile';
        },
        error: function(xhr, ajaxOptions, thrownError){
              alert(thrownError + '\n' + xhar.status + '\n' + ajaxOptions);
        }
    });
}