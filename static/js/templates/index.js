$(document).ready(function () {
    $('#psswdError').hide();
    $('#contactNameError').hide();
    $('#contactMailError').hide();

    $('#btnSend').click(function () {
        if($('#contactName').val().length <= 0){
            $('#contactNameError').show();
            return;
        }

        if($('#contactEmail').val().length <= 0){
            $('#contactMailError').show();
            return;
        }

        Login();
    });

    $('#contactName').click(function () {
        $('#contactNameError').hide();
        $('#psswdError').hide();
    });

    $('#contactEmail').click(function () {
        $('#contactMailError').hide();
        $('#psswdError').hide();
    });
});

function Login() {
        $.ajax({
        type: 'GET',
        url: '../responses/LogIn',
        data: {
            'mail': $('#contactName').val(),
            'psswd': $('#contactEmail').val()
        },
        success: function (response) {
            if(response === '-1'){
                $('#psswdError').html('User Not Found or Not Verified').show();
            }else if(response === '0'){
                $('#psswdError').html('Incorrect Password').show();
            }else{
                window.location.href = "./";
            }
        },
        error: function(xhr, ajaxOptions, thrownError){
              alert(thrownError + '\n' + xhar.status + '\n' + ajaxOptions);
        }
    });
}