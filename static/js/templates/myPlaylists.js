$( document ).ready(function() {
    $('#namePlaylistError').hide();

    let pageno = 1;
    let pageno2 = 1;
    fillHexagonsData(pageno);
    fillHexagonsData2(pageno2);

     $('#prev_control2').click(function(){
        pageno2--;
        fillHexagonsData2(pageno2);
    });

    $('#next_control2').click(function () {
       pageno2++;
       fillHexagonsData2(pageno2);
    });

    $('#prev_control').click(function(){
        pageno--;
        fillHexagonsData(pageno);
    });

    $('#next_control').click(function () {
       pageno++;
       fillHexagonsData(pageno);
    });

    $('#namePlaylist').click(function () {
        $('#namePlaylistError').hide();
    });

    $('#submitNewPlaylist').click(function () {
        if($('#namePlaylist').val().length <= 0){
            $('#namePlaylistError').show();
            return
        }

        createPlaylist();
    })
});

function fillHexagonsData2(pageno) {
    $.ajax({
    type: "GET",
    url: "../responses/PLAYLISTS2?page="+pageno+"&user="+$('#user').val(),
    data: {id: 1},
    dataType: "json",
    success: function(result){
        var html = "";
        var json = result;
        html += "<div class=\"row home-masonry-set\">";

        for(var i=0; i<json.length; i++){
            html += "<div id=\"div_"+i+"\" class=\"item col-md-4 \" onclick='goToPlaylist("+json[i].pk+")' style=\"display: none;\"'>";
            html += "       <img src=\"../static/images/default.png\" alt=\"\"/>\n";
            html += "<article>";
            html += "<h3>"+json[i].fields['NAME']+"</h3>";
            //html += "<p>"+json[i].fields['FECHANACIMIENTO']+"</p>\n";
            html += "<h4 style=\"color: #EA011E\"><i class=\"fa fa-play\" style='padding-right: 0em'></i> Go To</h4>";
            html += "</article>";
            html += "</div>";
        }

        $('#categories2').html(html);
        $('#pageno2').html(pageno);
        loadDivs2(json.length);
    }
  });
}

function fillHexagonsData(pageno){
  $.ajax({
    type: "GET",
    url: "../responses/PLAYLISTS?page="+pageno+"&user="+$('#user').val(),
    data: {id: 1},
    dataType: "json",
    success: function(result){
        var html = "";
        var json = result;
        html += "<div class=\"row home-masonry-set\">";

        for(var i=0; i<json.length; i++){
            html += "<div id=\"div"+i+"\" class=\"item col-md-4 \" onclick='goToPlaylist("+json[i].pk+")' style=\"display: none;\"'>";
            html += "       <img src=\"../static/images/default.png\" alt=\"\"/>\n";
            html += "<article>";
            html += "<h3>"+json[i].fields['NAME']+"</h3>";
            //html += "<p>"+json[i].fields['FECHANACIMIENTO']+"</p>\n";
            html += "<h4 style=\"color: #EA011E\"><i class=\"fa fa-play\" style='padding-right: 0em'></i> Go To</h4>";
            html += "</article>";
            html += "</div>";
        }

        $('#categories').html(html);
        $('#pageno').html(pageno);
        loadDivs(json.length);
    }
  });
}

function loadDivs(length) {
    for(var i = 1; i<=length; i++){
        $('#div'+(i-1)).fadeIn(500*i);
    }
}

function loadDivs2(length) {
    for(var i = 1; i<=length; i++){
        $('#div_'+(i-1)).fadeIn(500*i);
    }
}

function goToPlaylist(id){
    window.location.href = './playlist?id='+id;
}

function createPlaylist() {
    $.ajax({
       type: 'POST',
       url: '../responses/NEWPLAYLIST',
       data: $('#newPlaylist').serialize(),
       success: function (response) {
            pageno = 1;
            fillHexagonsData(pageno);
        },
        error: function(xhr, ajaxOptions, thrownError){
              alert(thrownError + '\n' + xhar.status + '\n' + ajaxOptions);
        }
    });
}