$( document ).ready(function() {
    fillTable();
});

function fillTable() {
     $('#songs tbody').html("");
    $.ajax({
        type: "GET",
        url: "../responses/PLAYLISTSONGS?playlist="+$('#playlist').val(),
        data: {id: 1},
        dataType: "json",
        success: function (result) {
            let songs = result;
            for (let i = 0; i < songs.length; i++) {
                addSong(songs[i], (i + 1));
            }
        }
    });

    function addSong(song, no) {
        console.log(song);
        const row = "" +
            "<tr>" +
            "     <td>" + no + "</td>" +
            "     <td>" + song['SONG__NAME'] + "</td>" +
            "     <td class=\"add\" onclick=\"deleteSong("+song['SONG_PLAYLIST']+")\">Delete</td>" +
            "</tr>";
        $('#songs tbody').append(row);
    }
}

function deleteSong(id) {
    if(confirm("Do you wanna delete this song?")){
        $.ajax({
        type: "GET",
        url: "../responses/DELSONGPLAY?song="+id,
        data: {id: 1},
        dataType: "json",
        success: function (result) {
           fillTable();
        }
    });
    }
}

function delPlaylist(){
    if(confirm("Do you wanna delete this playlist?")){
        $.ajax({
        type: "GET",
        url: "../responses/DELPLAYLIST?playlist="+$('#playlist').val(),
        data: {id: 1},
        dataType: "json",
        success: function (result) {
           window.location.href = './myplaylists';
        }
    });
    }
}