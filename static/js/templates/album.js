$( document ).ready(function() {
    fillTable();
});

function fillTable(){
    $('#songs tbody').html("");
  $.ajax({
    type: "GET",
    url: "../responses/SONGS?album="+$('#album').val(),
    data: {id: 1},
    dataType: "json",
    success: function(result) {
        let songs = result;
        for(let i=0; i<songs.length; i++){
            addSong(songs[i], (i+1));
        }
    }
  });

  function addSong(song, no){
      console.log(song);
      const row = "" +
          "<tr>" +
          "     <td>" + no + "</td>" +
          "     <td>" + song.fields['NAME'] + "</td>" +
          "     <td>" +
          "         <div class=\"dropdown\">\n" +
          "             <input type=\"button\" style=\"height: 2.0em; line-height: 0;\" onclick=\"addSong("+ song.pk +")\" value=\"ADD\" class=\"dropbtn\">\n" +
          "             <div id=\"myDropdown"+song.pk+"\" class=\"dropdown-content\">\n" +
          "             </div>" +
          "         </div>" +
          "     </td>" +
          "     <td class=\"add\" onclick=\"deleteSong("+song.pk+")\">Delete</td>"+
          "</tr>";
      $('#songs tbody').append(row);
  }
}

function deleteSong(id) {
    if(confirm("Do you wanna delete this song?")){
        $.ajax({
        type: "GET",
        url: "../responses/DELSONG?song="+id,
        data: {id: 1},
        dataType: "json",
        success: function (result) {
           fillTable();
        }
    });
    }
}

function addSong(id){
    //alert(id);
    $.ajax({
    type: "GET",
    url: "../responses/PLAYLISTS?user="+$('#user').val(),
    data: {id: 1},
    dataType: "json",
    success: function(result){
        var html = "";
        var json = JSON.parse(result);

        for(var i=0; i<json.length; i++){
            //json[i].pk
            //json[i].fields['NAME']
            html += "<a onclick='addTo("+json[i].pk+","+id+")'>"+json[i].fields['NAME']+"</a>";
        }
        $('#myDropdown'+id).html(html);
    }
  });

    document.getElementById("myDropdown"+id).classList.toggle("show");
}

function addTo(idPlaylist, idSong){
    $.ajax({
       type: 'GET',
       url: '../responses/NEWSONG',
       data:
           {
                'song': idSong,
                'playlist': idPlaylist
           },
        success: function () {
            alert("Song Added Successfully")
        },
        error: function(xhr, ajaxOptions, thrownError){
              alert(thrownError + '\n' + xhar.status + '\n' + ajaxOptions);
        }
    });
}

window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
      const dropdowns = document.getElementsByClassName("dropdown-content");
      let i;
      for (i = 0; i < dropdowns.length; i++) {
          const openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
        }
    }
  }
};


function delAlbum(){
    if(confirm("Do you wanna delete this album?")){
        $.ajax({
        type: "GET",
        url: "../responses/DELALBUM?album="+$('#album').val(),
        data: {id: 1},
        dataType: "json",
        success: function (result) {
           window.location.href = './artists';
        }
    });
    }
}