$( document ).ready(function() {
    let pageno = 1;
    fillHexagonsData(pageno);

    $('#prev_control').click(function(){
        pageno--;
        fillHexagonsData(pageno);
    });

    $('#next_control').click(function () {
       pageno++;
       fillHexagonsData(pageno);
    });
});


function fillHexagonsData(pageno){
  $.ajax({
    type: "GET",
    url: "../responses/ALBUMS?page="+pageno+"&artist="+$('#artist').val(),
    data: {id: 1},
    dataType: "json",
    success: function(result){
        var html = "";
        var json = result;
        html += "<div class=\"row home-masonry-set\">";

        for(var i=0; i<json.length; i++){
            html += "<div id=\"div"+i+"\" class=\"item col-md-4 \" onclick='goToAlbum("+json[i].pk+")' style=\"display: none;\"'>";
            html += "       <img src=\""+json[i].fields['PHOTO']+"\" alt=\"\"/>\n";
            html += "<article>";
            html += "<h3>"+json[i].fields['NAME']+"</h3>";
            //html += "<p>"+json[i].fields['FECHANACIMIENTO']+"</p>\n";
            html += "<h4 style=\"color: #EA011E\"><i class=\"fa fa-play\" style='padding-right: 0em'></i> Go To Album</h4>";
            html += "</article>";
            html += "</div>";
        }

        $('#categories').html(html);
        $('#pageno').html(pageno);
        loadDivs(json.length);
    }
  });
}

function loadDivs(length) {
    for(var i = 1; i<=length; i++){
        $('#div'+(i-1)).fadeIn(500*i);
    }
}

function goToAlbum(id){
    window.location.href = './album?album='+id;
}

function followArtist(){
    if(confirm("Do you wanna follow this artist?")){
        $.ajax({
        type: "GET",
        url: "../responses/FOLLOWARTIST?artist="+$('#artist').val(),
        data: {id: 1},
        success: function (result) {
           window.location.href = './artists';
        }
    });
    }
}

function unFollowArtist() {
    if(confirm("Do you wanna UnFollow this artist?")){
        $.ajax({
        type: "GET",
        url: "../responses/UNFOLLOWARTIST?artist="+$('#artist').val(),
        data: {id: 1},
        success: function (result) {
           window.location.href = './artists';
        }
    });
    }
}

function delArtist(){
    if(confirm("Do you wanna delete this artist?")){
        $.ajax({
        type: "GET",
        url: "../responses/DELARTIST?artist="+$('#artist').val(),
        data: {id: 1},
        dataType: "json",
        success: function (result) {
           window.location.href = './artists';
        }
    });
    }
}