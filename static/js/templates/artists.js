$( document ).ready(function() {
    let pageno = 1;
    fillHexagonsData(pageno);

    $('#prev_control').click(function(){
        pageno--;
        fillHexagonsData(pageno);
    });

    $('#next_control').click(function () {
       pageno++;
       fillHexagonsData(pageno);
    });

});




function createNew2(){


  var file_data = $('#archivo').prop('files')[0];
  var form_data = new FormData();
  form_data.append('photo', file_data);
  form_data.append('csrfmiddlewaretoken', $('input[name=csrfmiddlewaretoken]').val());


          $.ajax({

              type: 'POST',
              url: '../responses/newCoach',
              data: form_data,
              dataType: 'text',
              cache: false,
              contentType: false,
              processData: false,
              success: function(msg) {

                  // Message was sent
                  if (msg != 'OK') {
                      alert('okkkkkk');
                  }
                  // There was an error
                  else {
                      alert('eroooorr');
                  }

              },
              error: function(xhr, ajaxOptions, thrownError){
                  alert(thrownError + '\n' + xhr.status + '\n' + ajaxOptions);



              }

          });

}

function createNew(){
  $('#contactForm').validate({

      /* submit via ajax */
      submitHandler: function(form) {

          var sLoader = $('.submit-loader');
          var file_data = $('#archivo').prop('files')[0];
          var form_data = new FormData();
          form_data.append('file', file_data);
          form_data.append('csrfmiddlewaretoken', $('input[name=csrfmiddlewaretoken]').val());

          $.ajax({

            type: 'POST',
            url: '../responses/newCoach',
            data: form_data,
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
              beforeSend: function() {

                  sLoader.slideDown("slow");

              },
              success: function(msg) {

                  // Message was sent
                  if (msg != 'OK') {
                      sLoader.slideUp("slow");
                      $('.message-warning').fadeOut();
                      $('#contactForm').fadeOut();
                      $('.message-success').fadeIn();
                  }
                  // There was an error
                  else {
                      sLoader.slideUp("slow");
                      $('.message-warning').html(msg);
                      $('.message-warning').slideDown("slow");
                  }

              },
              error: function(xhr, ajaxOptions, thrownError){
                  alert(thrownError + '\n' + xhr.status + '\n' + ajaxOptions);

                  sLoader.slideUp("slow");
                  $('.message-warning').html("Something went wrong. Please try again.");
                  $('.message-warning').slideDown("slow");

              }

          });
      }

  });
}

function fillHexagonsData(pageno){
  $.ajax({
    type: "GET",
    url: "../responses/ARTISTS?page="+pageno,
    data: {id: 1},
    dataType: "json",
    success: function(result){
        var html = "";
        var json = result;
        html += "<div class=\"row home-masonry-set\">";

        for(var i=0; i<json.length; i++){
            html += "<div id=\"div"+i+"\" onclick=\"redirectToArtist("+json[i].pk+")\" class=\"item col-md-4 \" style=\"display: none;\"'>";
            html += "       <img src=\""+json[i].fields['PHOTO']+"\" alt=\"\"/>\n";
            html += "<article>";
            html += "<h3>"+json[i].fields['NAME']+"</h3>";
            //html += "<p>"+json[i].fields['FECHANACIMIENTO']+"</p>\n";
            html += "<h4 style=\"color: #EA011E\"><i class=\"fa fa-play\" style='padding-right: 0.5em'></i> Go To Artist</h4>";
            html += "</article>";
            html += "</div>";
        }

        $('#categories').html(html);
        $('#pageno').html(pageno);
        loadDivs(json.length);
    }
  });
}

function loadDivs(length) {
    for(var i = 1; i<=length; i++){
        $('#div'+(i-1)).fadeIn(500*i);
    }
}

function redirectToArtist(id){
    window.location.href = './artist?id='+id;
}
