from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.HomeView, name='index'),
    path('LogOut', views.LogOut, name='LogOut'),
    path('accessdenied', views.AccesDeniedView.as_view(), name='accessdenied'),
    path('register', views.RegisterView.as_view(), name='register'),
    # path('stadiums', views.StadiumView.as_view(), name='stadiums'),

    path('artists', views.ArtistsView, name='artists'),
    path('newCoach', views.NewCoachView, name='newCoach'),

    path('albums', views.AlbumsView, name='albums'),

    path('artist', views.SingleArtist, name='artist'),
    path('album', views.Album, name='album'),
    path('playlist', views.PlayList, name='playlist'),
    path('myplaylists', views.MyPlaylists, name='myplaylists'),
    path('profile', views.Profile, name='profile'),
    path('editprofile', views.editProfile, name='editprofile'),
    path('verify', views.verify, name='verify'),

    # path('teams', views.TeamsView.as_view(), name='teams'),
    # path('players', views.PlayersView.as_view(), name='players'),
    # path('matches', views.MatchesView.as_view(), name='matches'),
    # path('competitions', views.CompetitionsView.as_view(), name='competitions'),
]
