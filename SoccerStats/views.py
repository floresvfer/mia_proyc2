import json

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views.generic.base import TemplateView
from responses import models
from responses.models import ARTIST
from responses.models import ALBUM
from responses.models import USERR
from responses.models import SONG
from responses.models import PLAYLIST
from responses.models import SONG_PLAYLIST
from responses.models import USER_ARTIST
from responses.models import USER_PLAYLIST


def HomeView(request):
    members = USERR.objects.count()
    albums = ALBUM.objects.filter(STATUS=1).count()
    songs = SONG.objects.filter(STATUS=1).count()
    artists = ARTIST.objects.filter(STATUS=1).count()
    return render(request, 'index.html', {
        'members': members,
        'albums': albums,
        'songs': songs,
        'artists': artists,
    })


class AccesDeniedView(TemplateView):
    template_name = 'noaccess.html'


class RegisterView(TemplateView):
    template_name = 'register.html'


# class StadiumView(TemplateView):
#     template_name = 'stadiums.html'


# class CoacheView(TemplateView):
#     template_name = 'artists.html'


def ArtistsView(request):
    if 'TYPE' not in request.session:
        return render(request, 'noaccess.html')
    return render(request, 'artists.html')


def AlbumsView(request):
    if 'TYPE' not in request.session:
        return render(request, 'noaccess.html')
    return render(request, 'albums.html')


def NewCoachView(request):
    if 'TYPE' not in request.session:
        return render(request, 'noaccess.html')
    return render(request, 'newCoach.html')


def SingleArtist(request):
    if 'TYPE' not in request.session:
        return render(request, 'noaccess.html')

    object = ARTIST.objects.values('NAME', 'BIRTHDATE', 'PHOTO', 'PAIS__NOMBRE', 'ARTIST') \
        .prefetch_related('PAIS') \
        .get(ARTIST=request.GET.get('id'))

    object["BIRTHDATE"] = str(object["BIRTHDATE"])
    _obj = json.dumps(object)

    aver = 1
    try:
        USER_ARTIST.objects.get(USERR=request.session['USERR'], ARTIST=request.GET.get('id'), STATUS=1)
    except USER_ARTIST.DoesNotExist:
        aver = 0

    return render(request, 'artist.html',
                  {
                      'name': object['NAME'],
                      'photo': object['PHOTO'],
                      'birthdate': object['BIRTHDATE'],
                      'country': object['PAIS__NOMBRE'],
                      'artist': object['ARTIST'],
                      'aver': aver
                  })


def Profile(request):
    if 'TYPE' not in request.session:
        return render(request, 'noaccess.html')

    object = USERR.objects.values('USERR', 'NAME', 'TYPE_USER', 'BIRTHDATE', 'TYPE_USER__DESCRIPTION', 'PAIS__NOMBRE',
                                  'PHOTO') \
        .prefetch_related('PAIS', 'TYPE_USER') \
        .get(USERR=request.session['USERR'])

    return render(request, 'profile.html',
                  {
                      'name': object['NAME'],
                      'user': object['USERR'],
                      'birthdate': object['BIRTHDATE'],
                      'pais': object['PAIS__NOMBRE'],
                      'photo': object['PHOTO'],
                      'type': object['TYPE_USER__DESCRIPTION'],
                      'type_': object['TYPE_USER']
                  }
                  )


def editProfile(request):
    if 'TYPE' not in request.session:
        return render(request, 'noaccess.html')

    object = USERR.objects.values('NAME', 'LASTNAME', 'PHONE', 'ADRESS', 'PAIS', 'PHOTO') \
        .get(USERR=request.session['USERR'])

    return render(request, 'editProfile.html',
                  {
                      'name': object['NAME'],
                      'lastname': object['LASTNAME'],
                      'phone': object['PHONE'],
                      'adress': object['ADRESS'],
                      'photo': object['PHOTO']
                  }
                  )


def Album(request):
    if 'TYPE' not in request.session:
        return render(request, 'noaccess.html')
    object = ALBUM.objects.values('PHOTO', 'NAME', 'REALSEDATE', 'GENDER__DESCRIPTION', 'ARTIST', 'ALBUM') \
        .prefetch_related('GENDER') \
        .get(ALBUM=request.GET.get('album'))

    object["REALSEDATE"] = str(object["REALSEDATE"])

    _obj = json.dumps(object)

    return render(request, 'album.html',
                  {
                      'photo': object['PHOTO'],
                      'name': object['NAME'],
                      'gender': object['GENDER__DESCRIPTION'],
                      'realsedate': object['REALSEDATE'],
                      'description': object['GENDER__DESCRIPTION'],
                      'artist': object['ARTIST'],
                      'album': object['ALBUM']
                  })


def PlayList(request):
    if 'TYPE' not in request.session:
        return render(request, 'noaccess.html')

    object = PLAYLIST.objects.values('NAME', 'CREATEDDATE', 'PLAYLIST', 'USERR') \
        .get(PLAYLIST=request.GET.get('id'))

    object["CREATEDDATE"] = str(object["CREATEDDATE"])

    _obj = json.dumps(object)
    return render(request, 'playlist.html',
                  {
                      'name': object['NAME'],
                      'realsedate': object['CREATEDDATE'],
                      'playlist': object['PLAYLIST'],
                      'user': object['USERR']
                  })


def MyPlaylists(request):
    return render(request, 'myPlaylists.html')


# class TeamsView(TemplateView):
#     template_name = 'teams.html'


# class PlayersView(TemplateView):
#     template_name = 'players.html'


# class MatchesView(TemplateView):
#     template_name = 'matches.html'


# class CompetitionsView(TemplateView):
#     template_name = 'competitions.html'

# Create your views here.
def LogOut(request):
    if 'TYPE' in request.session:
        del request.session['TYPE']
        del request.session['USERR']
        del request.session['NAME']

    members = USERR.objects.count()
    albums = ALBUM.objects.filter(STATUS=1).count()
    songs = SONG.objects.filter(STATUS=1).count()
    artists = ARTIST.objects.filter(STATUS=1).count()
    return render(request, 'index.html', {
        'members': members,
        'albums': albums,
        'songs': songs,
        'artists': artists,
    })


def verify(request):
    user = USERR.objects.get(USERR=request.GET.get('id'))
    user.STATUS = 1
    user.save()
    return render(request, 'verify.html')
