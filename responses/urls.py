from django.urls import path

from . import views


urlpatterns = [
    path('PAISES', views.PAISES, name='PAISES'),
    path('newCoach', views.newCoach, name='newCoach'),
    path('ARTISTS', views.ARTISTS, name='ARTISTS'),
    path('ALBUMS', views.ALBUMS, name='ALBUMS'),
    path('DELALBUM', views.DELALBUM, name='DELALBUM'),
    path('PLAYLISTS', views.PLAYLISTS, name='PLAYLISTS'),
    path('PLAYLISTS2', views.PLAYLISTS2, name='PLAYLISTS2'),
    path('ARTIST', views.ONEARTIST, name='ONEARTIST'),
    path('DELARTIST', views.DELARTIST, name='DELARTIST'),
    path('DELPLAYLIST', views.DELPLAYLIST, name='DELPLAYLIST'),
    path('SONGS', views.SONGS, name='SONGS'),
    path('PLAYLISTSONGS', views.PLAYLIST_SONGS, name='PLAYLISTSONGS'),
    path('LogIn', views.LogIn, name='LogIn'),
    path('LogIn2', views.LogIn2, name='LogIn2'),
    path('NEWPLAYLIST', views.newPlaylist, name='NEWPLAYLIST'),
    path('NEWSONG', views.addSong, name='NEWSONG'),
    path('DELSONG', views.DELSONG, name='DELSONG'),
    path('DELSONGPLAY', views.DELSONGPLAY, name='DELSONGPLAY'),
    path('FOLLOWARTIST', views.followArtist, name='FOLLOWARTIST'),
    path('UNFOLLOWARTIST', views.unFollowArtist, name='UNFOLLOWARTIST'),
    path('UPDATEPROFILE', views.updateProfile, name='UPDATEPROFILE'),
    path('SEND', views.sendMail, name='SEND'),
]
