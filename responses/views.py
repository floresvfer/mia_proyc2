from datetime import date, datetime

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core import serializers
from django.core.files.storage import FileSystemStorage
from django.core.mail import EmailMessage
from django.core.paginator import Paginator
import json
from django.http import HttpResponse
from django.http import JsonResponse

# from responses.models import COACH
from responses.models import PAIS
# from responses.models import PLAYER
# from responses.models import POSICION
# from responses.models import USUARIO
from responses.models import ARTIST
from responses.models import ALBUM
from responses.models import SONG
from responses.models import GENDER_USER
from responses.models import GENDER
from responses.models import TYPE_USER
from responses.models import USERR
from responses.models import PLAYLIST
from responses.models import SONG_PLAYLIST
from responses.models import USER_PLAYLIST
from responses.models import USER_ARTIST


#
# def dbTest(request):
#     objects = USUARIO.objects.all()
#
#     users_list = serializers.serialize('json', objects)
#     return HttpResponse(JsonResponse(users_list, safe=False), content_type="application/json")

def SONGS(request):
    objects = SONG.objects.filter(ALBUM=request.GET.get('album'), STATUS=1).order_by('SONG')
    #    if 'playlist' in request.GET:
    #       get playlist songs
    _list = serializers.serialize('json', objects)
    return HttpResponse(_list, content_type="application/json")


def DELSONG(request):
    object = SONG.objects.get(SONG=request.GET.get('song'))
    object.STATUS = 0
    object.save()
    return HttpResponse("1")


def DELSONGPLAY(request):
    object = SONG_PLAYLIST.objects.get(SONG_PLAYLIST=request.GET.get('song'))
    object.STATUS = 0
    object.save()
    return HttpResponse("1")


def PLAYLIST_SONGS(request):
    objects = SONG_PLAYLIST.objects.values('SONG', 'SONG__NAME', 'SONG_PLAYLIST') \
        .filter(PLAYLIST=request.GET.get('playlist'), SONG__STATUS=1, STATUS=1) \
        .prefetch_related('SONG')

    lista = list()
    for a in objects:
        lista.append(a)

    # _list = serializers.serialize('json', objects)
    # return HttpResponse(JsonResponse(_list, safe=False), content_type="application/json")
    # _obj = json.dumps(lista)

    return HttpResponse(JsonResponse(lista, safe=False), content_type="application/json")


def ALBUMS(request):
    objects = ALBUM.objects.filter(STATUS=1, ARTIST__STATUS=1).prefetch_related('ARTIST').order_by('-REALSEDATE')
    if 'artist' in request.GET:
        objects = ALBUM.objects.filter(ARTIST=request.GET.get('artist'), STATUS=1).order_by('-REALSEDATE', '-NAME')

    paginator = Paginator(objects, 6)

    page = request.GET.get('page')
    objs = paginator.get_page(page)

    _list = serializers.serialize('json', objs)

    _list2 = serializers.serialize('json', objects)

    if 'page' in request.GET:
        return HttpResponse(_list, content_type="application/json")
    else:
        return HttpResponse(_list2, content_type="application/json")


def DELALBUM(request):
    object = ALBUM.objects.get(ALBUM=request.GET.get('album'))
    object.STATUS = 0
    object.save()
    return HttpResponse("1")


def PLAYLISTS(request):
    objects = PLAYLIST.objects.filter(STATUS=1).order_by('-CREATEDDATE')
    if 'user' in request.GET and request.GET.get('user') is not '1':
        objects = PLAYLIST.objects.filter(USERR=request.GET.get('user'), STATUS=1).order_by('-CREATEDDATE', '-PLAYLIST')

    paginator = Paginator(objects, 6)

    page = request.GET.get('page')
    objs = paginator.get_page(page)

    _list = serializers.serialize('json', objects)

    if 'page' in request.GET:
        _list = serializers.serialize('json', objs)

    return HttpResponse(_list, content_type="application/json")


def PLAYLISTS2(request):
    objects = PLAYLIST.objects.filter(STATUS=1).order_by('-CREATEDDATE')
    if 'user' in request.GET and request.GET.get('user') is not '1':
        objects = PLAYLIST.objects.filter(STATUS=1).exclude(USERR=request.GET.get('user')).order_by('-CREATEDDATE',
                                                                                                    '-PLAYLIST')

    paginator = Paginator(objects, 6)

    page = request.GET.get('page')
    objs = paginator.get_page(page)

    _list = serializers.serialize('json', objects)

    if 'page' in request.GET:
        _list = serializers.serialize('json', objs)

    return HttpResponse(_list, content_type="application/json")


def ARTISTS(request):
    objects = ARTIST.objects.filter(STATUS=1)
    paginator = Paginator(objects, 6)

    page = request.GET.get('page')
    objs = paginator.get_page(page)

    _list = serializers.serialize('json', objects)

    if 'page' in request.GET:
        _list = serializers.serialize('json', objs)

    return HttpResponse(_list, content_type="application/json")


def DELARTIST(request):
    object = ARTIST.objects.get(ARTIST=request.GET.get('artist'))
    object.STATUS = 0
    object.save()
    return HttpResponse("1")


def DELPLAYLIST(request):
    object = PLAYLIST.objects.get(PLAYLIST=request.GET.get('playlist'))
    object.STATUS = 0
    object.save()
    return HttpResponse("1")


def ONEARTIST(request):
    object = ARTIST.objects.values('NAME', 'BIRTHDATE', 'PHOTO', 'PAIS__NOMBRE', 'ARTIST') \
        .prefetch_related('PAIS') \
        .get(ARTIST=request.GET.get('artist'))

    object["BIRTHDATE"] = str(object["BIRTHDATE"])
    _obj = json.dumps(object)

    return HttpResponse(JsonResponse(_obj, safe=False), content_type="application/json")


# def POSICIONES(request):
#     objects = POSICION.objects.all()
#
#     _list = serializers.serialize('json', objects)
#     return HttpResponse(JsonResponse(_list, safe=False), content_type="application/json")


def PAISES(request):
    objects = PAIS.objects.filter(STATUS=1)

    paises_list = serializers.serialize('json', objects)
    return HttpResponse(JsonResponse(paises_list, safe=False), content_type="application/json")


def updateProfile(request):
    if request.method == 'POST':
        user = USERR.objects.get(USERR=request.session['USERR'])
        name = request.POST['contactName']
        lastname = request.POST['contactLastName']
        password = request.POST['contactPassword']
        phone = request.POST['contactPhoneNumber']
        adress = request.POST['contactAdress']
        photo = request.POST['prevPhoto']

        if request.FILES['contactPhoto']:
            myfile = request.FILES['contactPhoto']
            myfile.name = '1'
            fs = FileSystemStorage(
                location='/home/flores08/Music/mySite/mysite/static/files/profiles')  # defaults to   MEDIA_ROOT
            filename = fs.save('2', myfile)
            photo = fs.url(filename)

        user.NAME = name
        user.LASTNAME = lastname
        user.PASSWORD = password
        user.PHONE = phone
        user.ADRESS = adress
        user.PHOTO = photo

        user.save()

        return HttpResponse("User Updated Succesfully")


def newCoach(request):
    if request.method == 'POST' and request.FILES['contactPhoto']:
        myfile = request.FILES['contactPhoto']
        myfile.name = '1'
        fs = FileSystemStorage(
            location='/home/flores08/Music/mySite/mysite/static/files/profiles')  # defaults to   MEDIA_ROOT
        filename = fs.save('2', myfile)

        pais = PAIS.objects.get(PAIS=request.POST['slcPais'])
        typeuser = TYPE_USER.objects.get(TYPE_USER=3)
        name = request.POST['contactName']
        lastname = request.POST['contactLastName']
        password = request.POST['contactPassword']
        mail = request.POST['contactEmail']
        phone = request.POST['contactPhoneNumber']
        birthdate = str(request.POST['contactBirth'])
        photo = fs.url(filename)
        registerdate = str(datetime.today().strftime('%Y-%m-%d'))
        adress = request.POST['contactAdress']
        status = 2
        genderuser = GENDER_USER.objects.get(GENDER_USER=request.POST['slcGender'])

        userr = \
            USERR(
                PAIS=pais,
                GENDER_USER=genderuser,
                TYPE_USER=typeuser,
                NAME=name,
                LASTNAME=lastname,
                PASSWORD=password,
                MAIL=mail,
                PHONE=phone,
                PHOTO=photo,
                BIRTHDATE=birthdate,
                REGISTERDATE=registerdate,
                ADRESS=adress,
                STATUS=status
            )
        userr.save()
        return HttpResponse("Playlist Created Successfully")


def newPlaylist(request):
    if request.method == 'POST':
        user = USERR.objects.get(USERR=request.POST['user'])
        playlist = \
            PLAYLIST(
                USERR=user,
                NAME=request.POST['namePlaylist'],
                CREATEDDATE=str(datetime.today().strftime('%Y-%m-%d')),
                STATUS=1
            )
        playlist.save()
        return HttpResponse("Playlist Created Successfully")
    else:
        return HttpResponse("0")


def addSong(request):
    if request.method == 'GET':
        song = SONG.objects.get(SONG=request.GET.get('song'))
        playlist = PLAYLIST.objects.get(PLAYLIST=request.GET.get('playlist'))

        newSong = \
            SONG_PLAYLIST(
                SONG=song,
                PLAYLIST=playlist,
                STATUS=1
            )
        newSong.save()
        return HttpResponse("Song Added Succesfully")
    else:
        return HttpResponse("0")


def followArtist(request):
    if 'artist' in request.GET:
        user = USERR.objects.get(USERR=request.session['USERR'])
        artist = ARTIST.objects.get(ARTIST=request.GET.get('artist'))

        follow = \
            USER_ARTIST(
                USERR=user,
                ARTIST=artist,
                STATUS=1
            )

        follow.save()
        return HttpResponse("Artist Followed Succesfully")


def unFollowArtist(request):
    object = USER_ARTIST.objects.get(USERR=request.session['USERR'], ARTIST=request.GET.get('artist'), STATUS=1)
    object.STATUS = 0
    object.save()
    return HttpResponse("Artist UnFollowed Succesfully")


def LogIn(request):
    if request.method == 'GET' and 'mail' in request.GET and 'psswd' in request.GET:
        try:
            object = USERR.objects.values('PASSWORD', 'USERR', 'NAME', 'TYPE_USER').get(MAIL=request.GET.get('mail'),
                                                                                        STATUS=1)
            if object:
                if object['PASSWORD'] != request.GET.get('psswd'):
                    return HttpResponse("0")
                else:
                    request.session['USERR'] = object['USERR']
                    request.session['TYPE'] = object['TYPE_USER']
                    request.session['NAME'] = object['NAME']
                    return HttpResponse(str(object['USERR']) + "," + str(object['TYPE_USER']))
            else:
                return HttpResponse("-1")

        except USERR.DoesNotExist:
                return HttpResponse("-1")

    return HttpResponse("-1")


def LogIn2(request):
    if request.method == 'GET' and 'mail' in request.GET and 'psswd' in request.GET:
        try:
            object = USERR.objects.values('PASSWORD', 'USERR', 'NAME', 'TYPE_USER').get(MAIL=request.GET.get('mail'),
                                                                                        STATUS=1)
            if object:
                if object['PASSWORD'] != request.GET.get('psswd'):
                    return HttpResponse("[0]")
                else:
                    request.session['USERR'] = object['USERR']
                    request.session['TYPE'] = object['TYPE_USER']
                    request.session['NAME'] = object['NAME']
                    return HttpResponse("["+str(object['USERR'])+"]")
            else:
                return HttpResponse("[-1]")

        except USERR.DoesNotExist:
                return HttpResponse("[-1]")

    return HttpResponse("[-1]")


def sendMail(request):
    id = request.GET.get('id')
    html_content = render_to_string('mail.html', {'id': id})
    email = EmailMessage('Confirmation Account', html_content, to=['nbafernandoflores8@gmail.com'])
    email.send()
    return HttpResponse(id)




# def PLAYERS(request):
#     objects = PLAYER.objects.all()
#
#     _list = serializers.serialize('json', objects)
#     return HttpResponse(JsonResponse(_list, safe=False), content_type="application/json")
#     # return HttpResponse('hello');

# Create your views here.
