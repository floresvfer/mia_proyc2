from django.conf.urls import url

from . import views


urlpatterns = [
    url('PAISES', views.PAISES, name='PAISES'),
    url('newCoach', views.newCoach, name='newCoach'),
    url('ARTISTS', views.ARTISTS, name='ARTISTS'),
    url('ALBUMS', views.ALBUMS, name='ALBUMS'),
    url('ARTIST', views.ONEARTIST, name='ONEARTIST'),
    url('SONGS', views.SONGS, name='SONGS'),
    url('LogIn', views.LogIn, name='LogIn'),
]
