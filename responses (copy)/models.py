from django.db import models


class GENDER(models.Model):
    GENDER = models.IntegerField(primary_key=True, blank=True, null=True)
    DESCRIPTION = models.CharField(max_length=50)
    STATUS = models.IntegerField()

    class Meta:
        db_table = "GENDER"


class PAIS(models.Model):
    PAIS = models.IntegerField(primary_key=True, blank=True, null=True)
    NOMBRE = models.CharField(max_length=50)
    STATUS = models.IntegerField()

    class Meta:
        db_table = "PAIS"


class ARTIST(models.Model):
    ARTIST = models.IntegerField(primary_key=True, blank=True, null=True)
    NAME = models.CharField(max_length=50)
    PAIS = models.ForeignKey(PAIS, on_delete=models.CASCADE)
    BIRTHDATE = models.DateField(['%d-%b-%y'])
    PHOTO = models.CharField(max_length=70)
    STATUS = models.IntegerField()

    class Meta:
        db_table = "ARTIST"


class ALBUM(models.Model):
    ALBUM = models.IntegerField(primary_key=True, blank=True, null=True)
    ARTIST = models.ForeignKey(ARTIST, on_delete=models.CASCADE)
    GENDER = models.ForeignKey(GENDER, on_delete=models.CASCADE)
    REALSEDATE = models.DateField(['%d-%b-%y'])
    PHOTO = models.CharField(max_length=70)
    NAME = models.CharField(max_length=50)
    STATUS = models.IntegerField()

    class Meta:
        db_table = "ALBUM"


class SONG(models.Model):
    SONG = models.IntegerField(primary_key=True, blank=True, null=True)
    ALBUM = models.IntegerField()
    ARTIST = models.IntegerField()
    GENDER = models.IntegerField()
    REALSEDATE = models.DateField(['%d-%b-%y'])
    NAME = models.CharField(max_length=100)
    STATUS = models.IntegerField()

    class Meta:
        db_table = "SONG"


class GENDER_USER(models.Model):
    GENDER_USER = models.IntegerField(primary_key=True, blank=True, null=True)
    DESCRIPTION = models.CharField(max_length=20)
    STATUS = models.IntegerField()

    class Meta:
        db_table = "GENDER_USER"


class TYPE_USER(models.Model):
    TYPE_USER = models.IntegerField(primary_key=True, blank=True, null=True)
    DESCRIPTION = models.CharField(max_length=20)
    STATUS = models.IntegerField()

    class Meta:
        db_table = "TYPE_USER"


class USERR(models.Model):
    USERR = models.IntegerField(primary_key=True, blank=True, null=True)
    GENDER_USER = models.ForeignKey(GENDER_USER, on_delete=models.CASCADE)
    PAIS = models.ForeignKey(PAIS, on_delete=models.CASCADE)
    TYPE_USER = models.ForeignKey(TYPE_USER, on_delete=models.CASCADE)
    NAME = models.CharField(max_length=50)
    LASTNAME = models.CharField(max_length=50)
    PASSWORD = models.CharField(max_length=50)
    MAIL = models.CharField(max_length=70)
    PHONE = models.CharField(max_length=15)
    PHOTO = models.CharField(max_length=70)
    BIRTHDATE = models.DateField()
    REGISTERDATE = models.DateField()
    ADRESS = models.CharField(max_length=50)
    STATUS = models.IntegerField()

    class Meta:
        db_table = "USERR"



# class POSICION(models.Model):
#    POSICION = models.IntegerField(primary_key=True, blank=True, null=True)
#    DESCRIPCION = models.CharField(max_length=25)
#
#    class Meta:
#       db_table = "POSICION"


# class USUARIO(models.Model):
#     USUARIO = models.IntegerField(primary_key=True, blank=True, null=True)
#     NOMBRE = models.CharField(max_length=20)
#     APELLIDO = models.CharField(max_length=20)
#     EDAD = models.IntegerField()
#
#     class Meta:
#         db_table = "USUARIO"

# class COACH(models.Model):
#     COACH = models.IntegerField(primary_key=True, blank=True, null=True)
#     PAIS = models.IntegerField()
#     NOMBRE = models.CharField(max_length=50)
#     APELLIDO = models.CharField(max_length=50)
#     FECHANACIMIENTO = models.DateField(['%d-%b-%y'])
#     FOTO = models.CharField(max_length=100)
#     STATUS = models.IntegerField()
#
#     class Meta:
#         db_table = "COACH"


# class PLAYER(models.Model):
#     PLAYER = models.IntegerField(primary_key=True, blank=True, null=True)
#     NOMBRE = models.CharField(max_length=50)
#     APELLIDO = models.CharField(max_length=50)
#     PAIS = models.IntegerField()
#     FOTO = models.CharField(max_length=100)
#     STATUS = models.IntegerField()
#     FECHANACIMIENTO = models.DateField(['%d-%b-%y'])
#
#     class Meta:
#         db_table = "PLAYER"

# Create your models here.
