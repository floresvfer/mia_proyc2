from django.core import serializers
from django.core.files.storage import FileSystemStorage
from django.core.paginator import Paginator
import json
from django.http import HttpResponse
from django.http import JsonResponse

# from responses.models import COACH
from responses.models import PAIS
# from responses.models import PLAYER
# from responses.models import POSICION
# from responses.models import USUARIO
from responses.models import ARTIST
from responses.models import ALBUM
from responses.models import SONG
from responses.models import GENDER_USER
from responses.models import GENDER
from responses.models import TYPE_USER
from responses.models import USERR


#
# def dbTest(request):
#     objects = USUARIO.objects.all()
#
#     users_list = serializers.serialize('json', objects)
#     return HttpResponse(JsonResponse(users_list, safe=False), content_type="application/json")

def SONGS(request):
    objects = SONG.objects.filter(ALBUM=request.GET.get('album'), STATUS=1).order_by('SONG')
    #    if 'playlist' in request.GET:
    #       get playlist songs
    _list = serializers.serialize('json', objects)
    return HttpResponse(JsonResponse(_list, safe=False), content_type="application/json")


def ALBUMS(request):
    objects = ALBUM.objects.filter(STATUS=1, ARTIST__STATUS=1).prefetch_related('ARTIST').order_by('-REALSEDATE')
    if 'artist' in request.GET:
        objects = ALBUM.objects.filter(ARTIST=request.GET.get('artist'), STATUS=1).order_by('-REALSEDATE', '-NAME')

    paginator = Paginator(objects, 6)

    page = request.GET.get('page')
    objs = paginator.get_page(page)

    _list = serializers.serialize('json', objs)
    return HttpResponse(JsonResponse(_list, safe=False), content_type="application/json")


def ARTISTS(request):
    objects = ARTIST.objects.filter(STATUS=1)
    paginator = Paginator(objects, 6)

    page = request.GET.get('page')
    objs = paginator.get_page(page)

    _list = serializers.serialize('json', objs)
    return HttpResponse(JsonResponse(_list, safe=False), content_type="application/json")


def ONEARTIST(request):
    object = ARTIST.objects.values('NAME', 'BIRTHDATE', 'PHOTO', 'PAIS__NOMBRE', 'ARTIST') \
        .prefetch_related('PAIS') \
        .get(ARTIST=request.GET.get('artist'))

    object["BIRTHDATE"] = str(object["BIRTHDATE"])
    _obj = json.dumps(object)

    return HttpResponse(JsonResponse(_obj, safe=False), content_type="application/json")


# def POSICIONES(request):
#     objects = POSICION.objects.all()
#
#     _list = serializers.serialize('json', objects)
#     return HttpResponse(JsonResponse(_list, safe=False), content_type="application/json")


def PAISES(request):
    objects = PAIS.objects.filter(STATUS=1)

    paises_list = serializers.serialize('json', objects)
    return HttpResponse(JsonResponse(paises_list, safe=False), content_type="application/json")


def newCoach(request):
    if request.method == 'POST' and request.FILES['photo']:
        myfile = request.FILES['photo']
        myfile.name = '1'
        fs = FileSystemStorage(location='/home/flores08/Pictures/site/coaches')  # defaults to   MEDIA_ROOT
        filename = fs.save('2', myfile)
        file_url = fs.url(filename)
        return HttpResponse(file_url)
    else:
        return HttpResponse(":(")


def LogIn(request):
    if request.method == 'GET' and 'mail' in request.GET and 'psswd' in request.GET:
        object = USERR.objects.values('PASSWORD', 'USERR', 'TYPE_USER').get(MAIL=request.GET.get('mail'))
        if object['PASSWORD'] != request.GET.get('psswd'):
            return HttpResponse("0")
        else:
            request.session['USERR'] = object['USERR']
            return HttpResponse(str(object['USERR']) + "," + str(object['TYPE_USER']))
    else:
        return HttpResponse("-1")

# def PLAYERS(request):
#     objects = PLAYER.objects.all()
#
#     _list = serializers.serialize('json', objects)
#     return HttpResponse(JsonResponse(_list, safe=False), content_type="application/json")
#     # return HttpResponse('hello');

# Create your views here.
